{
    "required": [
        "relayservice",
        "pubsub",
        "logger"
    ],
    "type": "object",
    "definitions": {},
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Configuration for pelops mqtt microservices.",
    "properties": {
        "monitoring-agent": {
            "required": [
                "restart-onboarding",
                "response-prefix",
                "onboarding-topic",
                "location",
                "room",
                "device",
                "name",
                "description",
                "log-level",
                "publish-log-level"
            ],
            "type": "object",
            "description": "Monitoring agent configuration",
            "properties": {
                "name": {
                    "type": "string",
                    "description": ""
                },
                "log-level": {
                    "type": "string",
                    "enum": [
                        "DEBUG",
                        "INFO",
                        "WARNING",
                        "ERROR",
                        "CRITICAL"
                    ],
                    "description": "Log level to be used."
                },
                "publish-log-level": {
                    "type": "string",
                    "enum": [
                        "DISABLED",
                        "DEBUG",
                        "INFO",
                        "WARNING",
                        "ERROR",
                        "CRITICAL"
                    ],
                    "description": "Log level for publishing log entries to the monitoring service. Use 'DISABLED' to disable this feature."
                },
                "onboarding-topic": {
                    "type": "string",
                    "description": ""
                },
                "location": {
                    "type": "string",
                    "description": ""
                },
                "gid": {
                    "type": [
                        "number",
                        "string"
                    ],
                    "description": ""
                },
                "device": {
                    "type": "string",
                    "description": ""
                },
                "restart-onboarding": {
                    "type": "integer",
                    "description": "timeout in seconds after which an restart onboarding request is restarted. example: 60 seconds."
                },
                "description": {
                    "type": "string",
                    "description": ""
                },
                "response-prefix": {
                    "type": "string",
                    "description": ""
                },
                "room": {
                    "type": "string",
                    "description": ""
                }
            }
        },
        "logger": {
            "required": [
                "log-level",
                "log-file"
            ],
            "type": "object",
            "description": "Logger configuration",
            "properties": {
                "log-rotation": {
                    "required": [
                        "maxbytes",
                        "backupcount"
                    ],
                    "type": "object",
                    "description": "parameters for log rotation (optional)",
                    "properties": {
                        "maxbytes": {
                            "type": "integer",
                            "description": "Rollover occurs whenever the current log file is nearly maxBytes in length",
                            "minvalue": 1
                        },
                        "backupcount": {
                            "type": "integer",
                            "description": "With a backupCount of 5 and a base file name of app.log, you would get app.log, app.log.1, app.log.2, up to app.log.5.",
                            "minvalue": 1
                        }
                    }
                },
                "log-level": {
                    "type": "string",
                    "enum": [
                        "DEBUG",
                        "INFO",
                        "WARNING",
                        "ERROR",
                        "CRITICAL"
                    ],
                    "description": "Log level to be used."
                },
                "log-file": {
                    "type": "string",
                    "description": "File name for the logger."
                }
            }
        },
        "pubsub": {
            "type": "object",
            "description": "pubsub servuce configuration",
            "additionalItems": false,
            "oneOf": [
                {
                    "required": [
                        "type",
                        "mqtt-address",
                        "mqtt-port"
                    ],
                    "additionalItems": false,
                    "properties": {
                        "type": {
                            "type": "string",
                            "enum": [
                                "MQTT",
                                "mqtt",
                                "Mqtt"
                            ],
                            "description": "which pub-sub-service should be used"
                        },
                        "mqtt-password": {
                            "type": "string",
                            "description": "Password for mqtt broker (optional)."
                        },
                        "credentials-file": {
                            "type": "string",
                            "description": "File containing the credentials (optional)."
                        },
                        "retain-messages": {
                            "type": "boolean",
                            "description": "Signal mqtt broker that messages should be retained."
                        },
                        "mqtt-port": {
                            "exclusiveMinimum": true,
                            "type": "integer",
                            "description": "Port of mqtt broker",
                            "minimum": 0
                        },
                        "mqtt-user": {
                            "type": "string",
                            "description": "User name for mqtt broker (optional)."
                        },
                        "log-level": {
                            "type": "string",
                            "enum": [
                                "DEBUG",
                                "INFO",
                                "WARNING",
                                "ERROR",
                                "CRITICAL"
                            ],
                            "description": "Log level to be used (optional)."
                        },
                        "qos": {
                            "type": "integer",
                            "description": "Set quality of service for subscribe, publish, and last will.",
                            "maximum": 2,
                            "minimum": 0
                        },
                        "mqtt-address": {
                            "type": "string",
                            "description": "URL of mqtt broker"
                        }
                    }
                },
                {
                    "required": [
                        "type"
                    ],
                    "additionalItems": false,
                    "properties": {
                        "type": {
                            "type": "string",
                            "enum": [
                                "QUEUE",
                                "queue",
                                "Queue"
                            ],
                            "description": "which pub-sub-service should be used"
                        },
                        "log-level": {
                            "type": "string",
                            "enum": [
                                "DEBUG",
                                "INFO",
                                "WARNING",
                                "ERROR",
                                "CRITICAL"
                            ],
                            "description": "Log level to be used (optional)."
                        }
                    }
                }
            ]
        },
        "relayservice": {
            "items": {
                "oneOf": [
                    {
                        "required": [
                            "active",
                            "name",
                            "type",
                            "topic-sub",
                            "topic-pub"
                        ],
                        "type": "object",
                        "properties": {
                            "type": {
                                "type": "string",
                                "enum": [
                                    "forward"
                                ],
                                "description": "[forward, echo, collect, distribute, multiply]"
                            },
                            "replace-message": {
                                "type": "string",
                                "description": "use this message instead of received message (optional)"
                            },
                            "name": {
                                "type": "string",
                                "description": "unique name for relay event"
                            },
                            "topic-sub": {
                                "type": "string",
                                "description": "forward messages published to this topic"
                            },
                            "topic-pub": {
                                "type": "string",
                                "description": "publish incoming messages to this topic"
                            },
                            "active": {
                                "type": "boolean",
                                "description": "entry ignored if set to False"
                            }
                        },
                        "additionalProperties": false
                    },
                    {
                        "required": [
                            "active",
                            "name",
                            "type",
                            "topic"
                        ],
                        "type": "object",
                        "properties": {
                            "name": {
                                "type": "string",
                                "description": "unique name for relay event"
                            },
                            "type": {
                                "type": "string",
                                "enum": [
                                    "echo"
                                ],
                                "description": "[forward, echo, collect, distribute, multiply]"
                            },
                            "replace-message": {
                                "type": "string",
                                "description": "use this message instead of received message (optional)"
                            },
                            "active": {
                                "type": "boolean",
                                "description": "entry ignored if set to False"
                            },
                            "topic": {
                                "type": "string",
                                "description": "all messages incoming on this topic are published back to this topic."
                            }
                        },
                        "additionalProperties": false
                    },
                    {
                        "required": [
                            "active",
                            "name",
                            "type",
                            "topics-sub",
                            "topic-pub"
                        ],
                        "type": "object",
                        "properties": {
                            "type": {
                                "type": "string",
                                "enum": [
                                    "collect"
                                ],
                                "description": "[forward, echo, collect, distribute, multiply]"
                            },
                            "replace-message": {
                                "type": "string",
                                "description": "use this message instead of received message (optional)"
                            },
                            "topics-sub": {
                                "items": {
                                    "type": "string"
                                },
                                "type": "array",
                                "description": "forward messages published to these topics"
                            },
                            "name": {
                                "type": "string",
                                "description": "unique name for relay event"
                            },
                            "topic-pub": {
                                "type": "string",
                                "description": "publish incoming messages to this topic"
                            },
                            "active": {
                                "type": "boolean",
                                "description": "entry ignored if set to False"
                            }
                        },
                        "additionalProperties": false
                    },
                    {
                        "required": [
                            "active",
                            "name",
                            "type",
                            "topic-sub",
                            "topics-pub"
                        ],
                        "type": "object",
                        "properties": {
                            "topics-pub": {
                                "items": {
                                    "type": "string"
                                },
                                "type": "array",
                                "description": "publish incoming messages to these topics"
                            },
                            "type": {
                                "type": "string",
                                "enum": [
                                    "distribute"
                                ],
                                "description": "[forward, echo, collect, distribute, multiply]"
                            },
                            "replace-message": {
                                "type": "string",
                                "description": "use this message instead of received message (optional)"
                            },
                            "name": {
                                "type": "string",
                                "description": "unique name for relay event"
                            },
                            "topic-sub": {
                                "type": "string",
                                "description": "forward messages published to this topic"
                            },
                            "active": {
                                "type": "boolean",
                                "description": "entry ignored if set to False"
                            }
                        },
                        "additionalProperties": false
                    },
                    {
                        "required": [
                            "active",
                            "name",
                            "type",
                            "topics-sub",
                            "topics-pub"
                        ],
                        "type": "object",
                        "properties": {
                            "topics-pub": {
                                "items": {
                                    "type": "string"
                                },
                                "type": "array",
                                "description": "publish incoming messages to these topics"
                            },
                            "type": {
                                "type": "string",
                                "enum": [
                                    "multiply"
                                ],
                                "description": "[forward, echo, collect, distribute, multiply]"
                            },
                            "replace-message": {
                                "type": "string",
                                "description": "use this message instead of received message (optional)"
                            },
                            "topics-sub": {
                                "items": {
                                    "type": "string"
                                },
                                "type": "array",
                                "description": "forward messages published to these topics"
                            },
                            "name": {
                                "type": "string",
                                "description": "unique name for relay event"
                            },
                            "active": {
                                "type": "boolean",
                                "description": "entry ignored if set to False"
                            }
                        },
                        "additionalProperties": false
                    }
                ]
            },
            "type": "array",
            "description": "Root node for skeiron specific entries.",
            "additionalProperties": false
        }
    }
}